public class TV {

    private Channel[] channels = new Channel[0];

    public TV() {
        this.channels = new Channel[0];
    }

    public void addToTV(Channel channels) {
        Channel[] temp = new Channel[this.channels.length + 1];

        for (int i = 0; i < channels.length; i++) {
            temp[i] = this.channels[i];
            System.out.println(this.channels[i].getChannelName());
        }
    }

    public Channel[] getChannels() {
        return this.channels;
    }
}
