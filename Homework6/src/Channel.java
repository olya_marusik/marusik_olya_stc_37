public class Channel {

    private String channelName;
    private Program[] programs;

    public Channel(String channelName) {
        this.channelName = channelName;
        this.programs = new Program[0];
    }

    public void addToChannel(Program program) {
        Program[] temp = new Program[this.programs.length + 1];

        for (int i = 0; i < this.programs.length; i++) {
            temp[i] = this.programs[i];
            System.out.println(this.programs[i].getProgramName());
        }
    }

    public String getChannelName() {
        return this.channelName;
    }

    public Program[] getPrograms() {
        return this.programs;
    }
}
