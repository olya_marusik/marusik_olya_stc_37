import java.util.Scanner;
import java.util.Arrays;
class Program5{
	public static void main (String [] args){
		Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int array [] = new int [n];
		int temp;

		for (int i = 0; i < array.length; i++){
			array[i] = scanner.nextInt();
		}
	System.out.println("array before bubble sorting: " + Arrays.toString(array));

		for (int i = 0; i < array.length; i++){
			for (int j = 0; j < array.length - 1; j++){
				if (array[j] > array[j + 1]){
					temp = array[j];
					array[j ] = array[j + 1];
					array[j + 1] = temp;
           		 }
        	}
        }	
	System.out.println("array after bubble sorting in ascending order: " + Arrays.toString(array));
		
		for (int i = 1; i < array.length; i++){
			for (int j = 0; j < array.length - 1; j++){
				if (array[j] < array[j + 1]){
					temp = array[j];
					array[j] = array[j + 1];
					array[j + 1] = temp;
            	}
        	}
    	}
	System.out.println("array after bubble sorting in descending order: " + Arrays.toString(array));	
		
	}
}

		

