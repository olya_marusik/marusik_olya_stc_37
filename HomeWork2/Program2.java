import java.util.Scanner;
import java.util.Arrays;
class Program2{
	public static void main (String [] args){
		Scanner scanner = new Scanner(System.in);
		int n = scanner.nextInt();
		int array [] = new int [n];
		
		for (int i = 0; i < array.length; i++){
			array[i] = scanner.nextInt();
		}

		for (int i = 0; i < array.length/2; i++){	
			int j = array[i];
 			array[i] = array[array.length -i - 1];
 			array[array.length -i - 1] = j;
		}
		System.out.println(Arrays.toString(array));
	}
}

