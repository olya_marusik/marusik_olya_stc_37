public class User {
    private String firstName;
    private String lastName;
    private int age;
    private boolean isWorker;

    private User(User.Builder builder) {
        this.firstName = builder.firstName;
        this.lastName = builder.lastName;
        this.age = builder.age;
        this.isWorker = builder.isWorker;
    }

    public static User.Builder builder() {
        return new User.Builder();
    }

    public String getFirstName() {
        return this.firstName;
    }

    public String getLastName() {
        return this.lastName;
    }

    public int getAge() {
        return this.age;
    }

    public boolean isWorker() {
        return this.isWorker;
    }

    public static class Builder {
        private String firstName;
        private String lastName;
        private int age;
        private boolean isWorker;

        public Builder() {
        }

        public User.Builder firstName(String firstName) {
            this.firstName = firstName;
            return this;
        }

        public User.Builder lastName(String lastName) {
            this.lastName = lastName;
            return this;
        }

        public User.Builder age(int age) {
            if (age > 0) {
                this.age = age;
            }

            return this;
        }

        public User.Builder isWoker(boolean isWorker) {
            this.isWorker = isWorker;
            return this;
        }

        public User build() {
            return new User(this);
        }
    }
}
