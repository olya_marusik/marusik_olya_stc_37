
class Program1{
	public static int getSumOfElements(int array[], int size){
		Scanner scanner = new Scanner(System.in);
		int arraySum = 0;

		for (int i = 0; i < array.length; i++){
			array[i] = scanner.nextInt();
			arraySum += array[i];
			}
		return arraySum;
	}

	public static void printReversalArray (int array[], int size){
		Scanner scanner = new Scanner(System.in);
		
		for (int i = array.length - 1; i >= 0; i--){
			array[i] = scanner.nextInt();
		}
		System.out.println(Arrays.toString(array));
	}

	public static double getArrayAverage(int array[], int size){
		Scanner scanner = new Scanner(System.in);
		int arraySum = 0;
		double arrayAverage = 0;

		for (int i = 0; i < array.length; i++){
			array [i] = scanner.nextInt();
			arraySum += array[i];
			arrayAverage = arraySum / size;
		}
		return arrayAverage;
	}
	
	public static void printChangeMaxMin (int array[], int size){
		Scanner scanner = new Scanner(System.in);
		int min = array[0];
		int max = array[0];
		int temp;
		int positionOfMin = 0;
		int positionOfMax = 0;

		for (int i = 0; i < array.length; i++){
			array[i] = scanner.nextInt();
			if (array[i] < min){
				min = array[i];
				positionOfMin = i;
				
			}
			if (array[i] > max){
				max = array[i];
				positionOfMax = i;
			}
		}

		temp = array[positionOfMax];
		array[positionOfMax] = array[positionOfMin];
		array[positionOfMin] = temp;

		System.out.println(Arrays.toString(array));
	}
	public static void printBubbleSorting (int array[], int size){
		Scanner scanner = new Scanner(System.in);
		int temp;
		boolean is_swap = false;

		for (int i = 0; i < array.length; i++){
			array[i] = scanner.nextInt();
		}
		
			for (int i = 0; i < array.length-2; i++){
				is_swap = false;
			for (int j = array.length-2; j >= i; j--){
				if (array[j] > array[j + 1]){
					temp = array[j + 1];
					array [j + 1] = array[j];
					array[j] = temp;
					is_swap = true;
				}
			}
		}
	System.out.println("array after bubble sorting in ascending order: " + Arrays.toString(array));

			for (int i = array.length-1; i >= 0; i--){
				is_swap = false;
			for (int j = 0; j < i; j++){
				if (array[j] < array[j + 1]){
					temp = array[j + 1];
					array [j + 1] = array[j];
					array[j] = temp;
					is_swap = true;
				}
			}
		}
	System.out.println("array after bubble sorting in descending order: " + Arrays.toString(array));

	}

	public static int convertToNumber(int array[], int size){
		Scanner scanner = new Scanner(System.in);
		int number = 0;

		for (int i = 0; i < array.length;  i++) {
          number *= 10;
          number += array[i];
        }
        return number;
	}

	public static void main(String [] args){
		Scanner scanner = new Scanner(System.in);
		int size = scanner.nextInt();
		int array [] = new int [size];

		int arraySum = getSumOfElements(array, size); 
		System.out.println("SumOfElements " + arraySum);

		printReversalArray(array, size);

		double arrayAverage = getArrayAverage(array, size);
		System.out.println("AverageOfElements " + arrayAverage);
		
		printChangeMaxMin(array, size);

		printBubbleSorting(array, size);

		int number = convertToNumber(array, size); 
		System.out.println("ConvertToNumber " + number);
	}
}

