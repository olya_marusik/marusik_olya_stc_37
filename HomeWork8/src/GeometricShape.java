public abstract class GeometricShape implements ShapeScale, MovingCenter {
    private String name;
    private int x = 0;
    private int y = 0;

    public GeometricShape(String name) {
        this.name = name;
    }

    @Override
    public void scale(int k) {
    }

    public abstract double perimeter();

    public abstract double area();

    @Override
    public void move(int x, int y) {
        System.out.println("Новые координаты центра фигуры: x = " + x + ", y = " + y);
    }

    public String getName() {
        return name;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

}
