public interface MovingCenter {
    void move(int x, int y);
}
