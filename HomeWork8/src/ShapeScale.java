public interface ShapeScale {
    void scale(int k);
}
