import java.util.Scanner;
class Program32{
	public static boolean isPrime(int primeNumberSum){
		if (primeNumberSum == 1 || primeNumberSum == 2 || primeNumberSum == 3){
			return true;
		}
		for (int i = 2; i*i <= primeNumberSum; i++){
			if (primeNumberSum % i == 0){
				return false;
			}
		}
		return true;
	}

	public static void main (String[] args){
		System.out.println("Enter number: ");
		Scanner scanner = new Scanner (System.in);
		int currentNumber = scanner.nextInt();
		int comp = 1;

		while(currentNumber != 0){

			int primeNumberSum = 0;
			int saveCurrentNumber = currentNumber;
		
			while (currentNumber != 0){
				primeNumberSum += currentNumber % 10;  
   				currentNumber = currentNumber / 10;
			}	
		System.out.println(primeNumberSum);

		boolean a = isPrime(primeNumberSum);

			if (a == true){
				comp *= saveCurrentNumber; 
			}
		
		currentNumber = scanner.nextInt();
	}
	System.out.println("Answer = " + comp);
	}
}