public class Main {
    public static void main(String[] args) {
        int[] array1 = {1234};
        NumbersAndStringProcessor numbers1 = new NumbersAndStringProcessor(array1);

        NumberProcess numberProcess1 = number -> {
            // развернуть число
            int processedNumber = 0;
            while (number != 0) {
                processedNumber = processedNumber * 10;
                processedNumber = processedNumber + number % 10;
                number = number / 10;
            }
            return processedNumber;
        };
        numbers1.process(numberProcess1);

        int[] array2 = {102030};
        NumbersAndStringProcessor numbers2 = new NumbersAndStringProcessor(array2);

        NumberProcess numberProcess2 = number -> {
            // убрать нули из числа

            // отделенная цифра
            int processedNumber = 0;
            // сохранить отделенную цифру после условия
            int saveNumber = 0;
            // коэффициент, на который будет умножена отделенная цифра после условия
            int a = 1;

            while (number != 0) {
                processedNumber = number % 10;

                if (processedNumber != 0) {
                    // к сохраненному числу добавить отделенную цифру после условия, умноженную на коэффициент
                    saveNumber += processedNumber * a;
                    // коэффициент увеличивается в 10 раз
                    a = a * 10;
                }
                number = number / 10;
            }
            return saveNumber;
        };
        numbers2.process(numberProcess2);


        int[] array3 = {521};
        NumbersAndStringProcessor numbers3 = new NumbersAndStringProcessor(array3);

        NumberProcess numberProcess3 = number -> {
            //заменить нечетный цифры

            // отделенная цифра
            int processedNumber = 0;
            // сохранить отделенную цифру после условия
            int saveNumber = 0;
            // коэффициент, на который будет умножена отделенная цифра после условия
            int a = 1;

            while (number != 0) {
                processedNumber = number % 10;

                if (processedNumber % 2 != 0) {
                    processedNumber = processedNumber - 1;
                }
                // к сохраненному числу добавить отделенную цифру после условия, умноженную на коэффициент
                saveNumber += processedNumber * a;
                // коэффициент увеличивается в 10 раз
                a = a * 10;
                number = number / 10;
            }
            return saveNumber;
        };

        numbers3.process(numberProcess3);


        String[] arraysS1 = {"Hello"};
        NumbersAndStringProcessor strings1 = new NumbersAndStringProcessor(arraysS1);

        StringProcess stringProcess1 = string -> {
            // развернуть строку
            char[] charArray = string.toCharArray();
            String processedStrings = "";
            for (int i = charArray.length - 1; i >= 0; i--) {
                processedStrings += charArray[i];
            }
            return processedStrings;
        };

        strings1.process(stringProcess1);

        String[] arraysS2 = {"Hello5"};
        NumbersAndStringProcessor strings2 = new NumbersAndStringProcessor(arraysS2);

        StringProcess stringProcess2 = string -> {
            // убрать все цифры из строки
            char[] charArray = string.toCharArray();
            String processedStrings = "";
            for (int i = 0; i < charArray.length; i++) {
                processedStrings += charArray[i];
            }
            processedStrings = processedStrings.replaceAll("\\d", "");

            return processedStrings;
        };

        strings2.process(stringProcess2);

        String[] arraysS3 = {"hello"};
        NumbersAndStringProcessor strings3 = new NumbersAndStringProcessor(arraysS3);

        StringProcess stringProcess3 = string -> {
            // сделать все буквы большими
            return string.toUpperCase();
        };

        strings3.process(stringProcess3);

    }
}
