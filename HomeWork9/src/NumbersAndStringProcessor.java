public class NumbersAndStringProcessor {
    // массив для хранения чисел
    private int processedNumbers[];
    // массив для хранения строк
    private String processedStrings[];


    // конструктор принимает на вход - массив чисел
    public NumbersAndStringProcessor(int[] processedNumbers) {
        this.processedNumbers = processedNumbers;
    }

    // конструктор принимает на вход - массив строк
    public NumbersAndStringProcessor(String[] processedStrings) {
        this.processedStrings = processedStrings;
    }


    // метод обработки числа
    public int[] process(NumberProcess process) { // экземпляр интерфейса
        // новый массив для обработанных чисел
        int newProcessedNumber[] = new int[processedNumbers.length];
        for (int i = 0; i < this.processedNumbers.length; i++) {
            // скопировать в новый массив все элементы из старого
            newProcessedNumber[i] = process.process(processedNumbers[i]);
            // применение метода интерфейса ко всем элементам старого массива

            // показать обработанные числа
            System.out.println(newProcessedNumber[i]);
        }
        // вернуть новый массив с обработанными элементами
        return newProcessedNumber;
    }


    // метод обработки строк
    public String[] process(StringProcess process) { // экземпляр интерфейса
        // новый массив для обработанных чисел
        String newProcessedString[] = new String[processedStrings.length];
        for (int i = 0; i < this.processedStrings.length; i++) {
            // скопировать в новый массив все элементы из старого
            newProcessedString[i] = process.process(processedStrings[i]);
            // применение метода интерфейса ко всем элементам старого массива

            // показать обработанные строки
            System.out.println(newProcessedString[i]);
        }
        // вернуть новый массив с обработанными элементами
        return newProcessedString;
    }
}

