import java.util.List;

public interface CarsRepository {

    // найти машины по цвету или пробегу
    List<Long> findAllByColorOrMileage(String color, Long mileage);

    // количество уникальных моделей в ценовой политике от 700 до 800
    long countFindUnique();

    // вывести цвет автомобиля с минимальной стоимостью
    String findMinCost();

}
